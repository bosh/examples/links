# BOSH Link Examples - Self Linking

## Official Documentation
**[BOSH -> Links -> Self linking](https://bosh.io/docs/links/#self)**

## About this  example
This release contains a simple job that provides a link and then consumes that link within the job's `spec` file, like so:

```yaml
provides:
- name: peers
  type: self_link_peers

consumes:
- name: peers
  type: self_link_peers
```

As the official documentation states, this is useful for when you need to discover and communicate with other peers within the instance group.

To make the peer discovery even more seamless, we leverage the [implicit linking](https://bosh.io/docs/links/#implicit) capability of BOSH so that we don't have to explicitly write the `provides` and `consumes` statements in our **manifest**.

This allows our job definition to be as simple as:
```yaml
instance_groups:
  - name: self-link
    # ...
    instances: 2
    # ...
    jobs:
      - name: self-link-example
        release: self-linking
```

To illustrate the effects linking has, and the data that becomes available, the release then dumps the contents of the link by using the `link('peers')` method in one of its templates. It also dumps the contents of the `spec` variable, to show how linked data affects its contents.

**PLEASE NOTE:** The release expects [BOSH DNS](https://bosh.io/docs/dns/) to be enabled for all instance. Use of a [Runtime Config](https://bosh.io/docs/dns/) is recommended.

## Templating Results
Below are example outputs for the `spec.dump` and `link.dump` files after templating:

## `link.dump`
ERB:
```ruby
<%= link('peers').to_yaml %>
```
Output (with some clean-up for readability):
```yaml
---
instances:
- name: self-link
  index: 0
  id: 6b59b317-7bc7-440a-b5d0-bb1d61bc33c9
  az: z1
  address: 6b59b317-7bc7-440a-b5d0-bb1d61bc33c9.self-link.default.self-linking.bosh
  properties:
  bootstrap: true
- name: self-link
  index: 1
  id: 16987e84-3d62-4b06-983b-f69d98f9b322
  az: z1
  address: 16987e84-3d62-4b06-983b-f69d98f9b322.self-link.default.self-linking.bosh
  properties:
  bootstrap: false
properties:
group_name: self-link
group_type: instance-group
default_network: default
deployment_name: self-linking
root_domain: bosh
dns_encoder:
  az_hash:
    z1: 1
  service_groups:
    15:
      group_type: instance-group
      group_name: self-link
      deployment: self-linking
    16:
      group_type: link
      group_name: peers-self_link_peers
      deployment: self-linking
  short_dns_enabled: false
  link_dns_enabled: true
use_short_dns: false
```

## `spec.dump`
ERB:
```ruby
<%= spec.to_yaml %>
```
Output (with some clean-up for readability):
```yaml
---
deployment: self-linking
job:
  name: self-link
  templates:
  - name: self-link-example
    version: "5577a78465ad8ae3587e36031c37ba4d8f525f11979f06980a9e4ae74e41e57d"
    sha1: "sha256:060b19e3bd006894d8c901717dfb02bac4319575795e7a36d375a99e93357203"
    blobstore_id: f99af0e3-176d-44b6-a620-c46bddf040b8
    logs: []
  - name: bosh-dns
    version: "bef8e7f9f96164ec76d320641287edbadccfc093e1e5fa66439246c31937e16a"
    sha1: "sha256:678d94a857683d4419ff18285f793b24d654d9c8787beb9d7c0d875c592691fc"
    blobstore_id: "e0c54273-82b6-47ee-845d-2b4947766ca4"
    logs: []
  template: self-link-example
  version: "5577a78465ad8ae3587e36031c37ba4d8f525f11979f06980a9e4ae74e41e57d"
index: 0
bootstrap: true
name: self-link
id: "6b59b317-7bc7-440a-b5d0-bb1d61bc33c9"
az: z1
networks:
  default:
    type: manual
    ip: "10.1.2.211"
    netmask: "255.255.255.0"
    cloud_properties:
      name: SERVERS
    default:
    - dns
    - gateway
    dns:
    - "10.1.2.1"
    gateway: "10.1.2.1"
    dns_record_name: "0.self-link.default.self-linking.bosh"
properties_need_filtering: true
dns_domain_name: bosh
address: "6b59b317-7bc7-440a-b5d0-bb1d61bc33c9.self-link.default.self-linking.bosh"
persistent_disk: 0
properties:
links:
  peers:
    deployment_name: self-linking
    domain: bosh
    default_network: default
    instance_group: self-link
    properties:
    use_short_dns_addresses: false
    use_link_dns_names: true
    instances:
    - name: self-link
      id: "6b59b317-7bc7-440a-b5d0-bb1d61bc33c9"
      index: 0
      bootstrap: true
      az: z1
      address: "6b59b317-7bc7-440a-b5d0-bb1d61bc33c9.self-link.default.self-linking.bosh"
    - name: self-link
      id: "16987e84-3d62-4b06-983b-f69d98f9b322"
      index: 1
      bootstrap: false
      az: z1
      address: "16987e84-3d62-4b06-983b-f69d98f9b322.self-link.default.self-linking.bosh"
    group_name: peers-self_link_peers
ip: "10.1.2.211"
release:
  name: self-linking
  version: "0.0.1"
```

## `peers.dump`
ERB:
```ruby
result = {}

result["number_of_peers"] = link("peers").instances.count
result["peers"] = link("peers").instances.map do |instance|
    {
        "id" => instance.id,
        "name" => instance.name,
        "address" => instance.address,
        "index" => instance.index,
        "bootstrap" => instance.bootstrap,
        "az" => instance.az
    }
end

JSON.pretty_generate(result)
```

Output:
```json
{
  "number_of_peers": 2,
  "peers": [
    {
      "id": "6b59b317-7bc7-440a-b5d0-bb1d61bc33c9",
      "name": "self-link",
      "address": "6b59b317-7bc7-440a-b5d0-bb1d61bc33c9.self-link.default.self-linking.bosh",
      "index": 0,
      "bootstrap": true,
      "az": "z1"
    },
    {
      "id": "16987e84-3d62-4b06-983b-f69d98f9b322",
      "name": "self-link",
      "address": "16987e84-3d62-4b06-983b-f69d98f9b322.self-link.default.self-linking.bosh",
      "index": 1,
      "bootstrap": false,
      "az": "z1"
    }
  ]
}
```
---
**NOTE**

Notice how the `address` property is a BOSH DNS entry.
This is controlled, in this release, through the `features.use_dns_addresses` statement in the manifest.

Please see the [official documentation](https://bosh.io/docs/dns/#links) on how BOSH DNS can impact links.

---