# BOSH Link Examples - Simple Explicit Linking

## Official Documentation
**[BOSH -> Links -> Explicit Linking](https://bosh.io/docs/links/#explicit)**

## About this  example
This release contains a two jobs, a database and a web application.

The `database` job provides details about its connection, including a custom property (`port`), to its consumers.

The `web-app` job consumes the database connection to allow it to connect to the database instance dynamically.

Although we could have used implicit linking here for this simple setup, for the purpose of providing an example, explicit linking is used in the **manifest**, like so:

```yaml
instance_groups:
  - name: database
    instances: 1
    # ...
    jobs:
      - name: database
        provides:
          db: {as: app-database}

  - name: web-app
    instances: 1
    # ...
    jobs:
      - name: web-app
        consumes:
          db: {from: app-database}
```

To illustrate the effects linking has, and the data that becomes available, the release then dumps the contents of the link by using the `link('db')` method in one of its templates. It also dumps the contents of the `spec` variable, to show how linked data affects its contents.

**PLEASE NOTE:** The release expects [BOSH DNS](https://bosh.io/docs/dns/) to be enabled for all instance. Use of a [Runtime Config](https://bosh.io/docs/dns/) is recommended.

## Templating Results
Below are example outputs for the `spec.dump` and `link.dump` files after templating:

## `link.dump` (web-app instance)
ERB:
```ruby
<%= link('db').to_yaml %>
```
Output (with some clean-up for readability):
```yaml
---
instances:
- name: database
  index: 0
  id: c8b78fbb-b993-4c1b-9caa-b3607400160f
  az: z1
  address: c8b78fbb-b993-4c1b-9caa-b3607400160f.database.default.explicit-linking-simple.bosh
  properties:
  bootstrap: true
properties:
  port: 25441
group_name: database
group_type: instance-group
default_network: default
deployment_name: explicit-linking-simple
root_domain: bosh
dns_encoder:
  az_hash:
    z1: 1
  service_groups:
    17:
      group_type: instance-group
      group_name: database
      deployment: explicit-linking-simple
    18:
      group_type: instance-group
      group_name: web-app
      deployment: explicit-linking-simple
    19:
      group_type: link
      group_name: app-database-db_connection
      deployment: explicit-linking-simple
  short_dns_enabled: false
  link_dns_enabled: true
use_short_dns: false
```

## `spec.dump` (web-app instance)
ERB:
```ruby
<%= spec.to_yaml %>
```
Output (with some clean-up for readability):
```yaml
---
deployment: explicit-linking-simple
job:
  name: explicit-linking-simple
  templates:
  - name: web-app
    version: "03c0489c53918ee434af7a01d85b758e700ff25f3c16811c19ecd1cdbe622eaf"
    sha1: "sha256:a3c29904a336e31c042a846334522062c8f4e67c6c4bd251bcc2e1d3287d4871"
    blobstore_id: "c9da5ae2-8757-4ecc-8b5e-c13aeb81664d"
    logs: []
  - name: bosh-dns
    version: "bef8e7f9f96164ec76d320641287edbadccfc093e1e5fa66439246c31937e16a"
    sha1: "sha256:678d94a857683d4419ff18285f793b24d654d9c8787beb9d7c0d875c592691fc"
    blobstore_id: "e0c54273-82b6-47ee-845d-2b4947766ca4"
    logs: []
  template: web-app
  version: "03c0489c53918ee434af7a01d85b758e700ff25f3c16811c19ecd1cdbe622eaf"
index: 0
bootstrap: true
name: web-app
id: "903f68ba-86b1-4274-8f53-30a22fe2755b"
az: z1
networks:
  default:
    type: manual
    ip: "10.1.2.212"
    netmask: "255.255.255.0"
    cloud_properties:
      name: SERVERS
    default:
    - dns
    - gateway
    dns:
    - "10.1.2.1"
    gateway: "10.1.2.1"
    dns_record_name: "0.web-app.default.explicit-linking-simple.bosh"
properties_need_filtering: true
dns_domain_name: bosh
address: "903f68ba-86b1-4274-8f53-30a22fe2755b.web-app.default.explicit-linking-simple.bosh"
persistent_disk: 0
properties:
links:
  db:
    deployment_name: explicit-linking-simple
    domain: bosh
    default_network: default
    instance_group: database
    properties:
      port: 25441
    use_short_dns_addresses: false
    use_link_dns_names: true
    instances:
    - name: database
      index: 0
      id: c8b78fbb-b993-4c1b-9caa-b3607400160f
      az: z1
      address: c8b78fbb-b993-4c1b-9caa-b3607400160f.database.default.explicit-linking-simple.bosh
      properties:
      bootstrap: true
    group_name: app-database-db_connection
ip: "10.1.2.212"
release:
  name: explicit-linking-simple
  version: "0.0.1"
```
---
**NOTE**

Notice how the `address` property is a BOSH DNS entry.
This is controlled, in this release, through the `features.use_dns_addresses` statement in the manifest.

Please see the [official documentation](https://bosh.io/docs/dns/#links) on how BOSH DNS can impact links.

---